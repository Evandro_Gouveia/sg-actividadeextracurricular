<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Actividade extends Model
{
    private $nome;
    private $dataInscricaoInicio;
    private $dataInscricaoFim;
    private $dataInicio;
    private $dataFim;

    public function getNome(){
        return $this->nome;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function getDataInscricaoInicio(){
        return $this->dataInscricaoInicio;
    }


    public function setDataInscricaoInicio(Carbon $dataInscricaoInicio ){
        $this->dataInscricaoInicio = $dataInscricaoInicio;
    }

    public function setDataInscricaoFim(Carbon $dataInscricaoFim){
        $this->dataInscricaoFim = $dataInscricaoFim;
    }

    public function getDataInicio(){
        return $this->dataInicio;
    }


    public function setDataInicio(Carbon $dataInicio){
        $this->dataInicio = $dataInicio;
    }


    public function getDataFim(){
        return $this->dataFim;
    }

    public function setDataFim(Carbon $dataFim){
        $this->dataFim = $dataFim;
    }

    public static function getAvailableForInscricao(){
        $currentDate = Carbon::now()->format('Y-m-d');

        return static::where('dataInscricaoInicio', '<=', $currentDate)
                     ->where('dataInscricaoFim', '>=', $currentDate)
                     ->get();

    }

}
