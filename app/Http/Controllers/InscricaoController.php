<?php

namespace App\Http\Controllers;

use App\Actividade;
use App\Inscricao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InscricaoController extends Controller
{
    public function create(Request $request, int $id){



        try{
            $actividade = Actividade::findOrFail($id);

            if(Inscricao::getInscricaoByStudentId($request->numeroEstudante, $actividade->id)){
                throw new \Exception('O Estudante com o Número '.$request->numeroEstudante.' já efectuou uma inscrição nesta Actividade.');
            }

            DB::beginTransaction();
            $inscricao  = new Inscricao();
            $inscricao->dados_aluno      = $request->dados_aluno;
            $inscricao->numero_estudante = $request->numeroEstudante;
            $inscricao->actividade_id    = $actividade->id;

            $inscricao->save();

            DB::commit();
            return redirect()->back()->with('status','Inscrição foi feita com Sucesso');
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('alert',$exception->getMessage());
        }
    }
}
