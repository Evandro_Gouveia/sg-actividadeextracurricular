<?php

namespace App\Http\Controllers;

use App\Actividade;
use App\PaginaInicial;
use App\Traits\FileStoring;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaginaInicialController extends Controller
{
    use FileStoring;

    public function index(){
        try{
            $config = PaginaInicial::findOrFail(env('PAGINA_INICIAL'));
            $actividades   = Actividade::getAvailableForInscricao();
            return view('welcome',compact('actividades','config'));
        }catch (\Exception $exception){
            return $exception->getMessage();
        }

    }

    public function showConfigForm(){
        try{
            $config = PaginaInicial::findOrFail(env('PAGINA_INICIAL'));
            return view('PaginaInicial.configForm',compact('config'));
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function config(Request $request){
        DB::beginTransaction();
        try{

            $inicial                        = PaginaInicial::findOrFail(env('PAGINA_INICIAL'));
            $inicial->background_image_path = $request->bg_img;

            if($request->hasFile('bg_img')){
                $inicial->background_image_path = 'storage/bg_public/'.$this->storeFiles($request);
            }

            $inicial->texto_principal       = $request->texto_principal;
            $inicial->titulo                = $request->titulo;

            $inicial->save();

            DB::commit();

            return redirect()->back()->with('status', 'Actualizado com Sucesso');

        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('alert', $exception->getMessage());
        }
    }



}
