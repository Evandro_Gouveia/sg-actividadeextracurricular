<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscricao extends Model
{
    public static function getInscricaoByStudentId(int $numeroEstudante, int $actividade_id){
        return static::where('numero_estudante',$numeroEstudante)
                     ->where('actividade_id',$actividade_id)
                     ->first();
    }
}
