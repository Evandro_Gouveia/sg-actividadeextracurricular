<?php


namespace App\Traits;


use Illuminate\Http\Request;

trait FileStoring
{
    private function storeFiles(Request $request){
        if(!$request->hasFile('bg_img')){
            throw new \Exception('Não existem ficheiros a serem salvos');
        }

        $file = $request->file('bg_img');

        if($file->getSize() > env('FILE_SIZE') ){
            throw new \Exception('Tamanho do ficheiro é maior que 5mb. Selecione um ficheiro com Tamanho menor');
        }

        $extension = $file->getClientOriginalExtension();

        if(!(strtoupper($extension) === 'JPG' || $extension === 'JPEG' || $extension === 'PNG')){
            throw new \Exception('Tipo de ficheiro não é válido. Use ficheiro com as Extensões: JPEG, JPG, PNG');
        }

        $filename = time() . '.' .$extension;

        try{
            $file->storeAs('public/bg_public', $filename);
        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

        return $filename;

    }
}