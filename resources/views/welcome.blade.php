<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/public-home.css')}}">
    <link rel="stylesheet" href="{{asset('js/build/css/intlTelInput.css')}}">
    <title>ISPTEC - Actividade Extracurricular</title>
</head>
<body>

    @if (session('status'))
        <div class="alert alert-success alert-dismissable fade show text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('status')}}
        </div>
    @endif

    @if (session('alert'))
        <div class="alert alert-danger alert-dismissable fade show text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('alert')}}
        </div>
    @endif
    <section id="cover" style="background-image: url('{{asset($config->background_image_path)}}')">
        <div id="cover-caption">
            <div class="container">
                <div class="col-sm-10 offset-sm-1">
                    <h1>Actividades Extracurriculares</h1>
                    <h4><i>ISPTEC</i></h4>
                </div>
            </div>
            <br>
            <a href="#nav-main" class="btn btn-outline-warning btn-sm" role="button">&darr;</a>
        </div>
    </section>

    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary navbar-full" id="nav-main">
        <a href="#" class="navbar-brand">
            <img src="{{asset('img/logo.png')}}" class="logo" alt="SIGAE">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navnavbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li>
                    <a href="#" class="nav-link text-warning">Matematica<span class="sr-only">(current)</span></a>
                </li>
                <li>
                    <a href="#" class="nav-link text-warning">Português<span class="sr-only">(current)</span></a>
                </li>
                <li>
                    <a href="#" class="nav-link text-warning">Física<span class="sr-only">(current)</span></a>
                </li>
                <li>
                    <a href="#" class="nav-link text-warning">Química<span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <section>
        <div class="section-content">
            <div class="container">
                <h2>{{$config->titulo}}</h2>
                <p class="lead">{{$config->texto_principal}}</p>
            </div>
        </div>
    </section>


    @foreach($actividades as $actividade)
        <div>
            <section id="about">
                <div class="section-content">
                    <div class="container">
                        <div class="col-lg-6 col-md-6 col sm-6">
                            <div class="about-text">
                                <h3>{{$actividade->nome}}</h3>
                                <br>
                                <h6>Regras de Participação</h6>
                                <p class="lead">{{$actividade->regras}}</p>
                                <br>
                                <a href="#" class="btn btn-sm btn-outline-warning modalClass" data-toggle="modal" id="{{$actividade->id}}" data-target="#modal-{{$actividade->id}}">Inscrever-se</a>
                                <a href="#" class="btn btn-sm btn-outline-light">Saber mais...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="section-content">

                </div>
            </section>
        </div>
    @endforeach


    <footer id="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <p>Copyright &copy; 2018</p>
                    <p>Coded by <a href="#" class="text-warning">Evandro Gouveia</a></p>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <ul class="list-unstyled">
                        <li><a href="#" class="text-warning">ISPTEC</a></li>
                        <li><a href="#" class="text-warning">Facebook</a></li>
                        <li><a href="#" class="text-warning">Twitter</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <p>Copyright &copy; 2018</p>
                    <p>Coded by <a href="#" class="text-warning">Evandro Gouveia</a></p>
                </div>
            </div>
        </div>
        @include('partial.modalMatematica')
    </footer>
    <script type="text/javascript" src="{{asset('js/jquery/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/build/js/intlTelInput.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("a.modalClass").click(function () {
                var id = $(this).prop("id");

                var telInput            = $("#telefone-"+id+"");
                var numeroEstudante     = $("#numeroEstudante-"+id+"");
                var setValidCourseYears = $("#curso-"+id+"");
                var ano                 = $("#ano-"+id+"");
                var name                = $("#nome-"+id+"");
                var email               = $("#email-"+id+"");

                setValidCourseYears.change(function () {
                    if($(this).val().split(' ')[0] === 'Engenharia'){
                        $(ano).prop('max','5')
                    }else{
                        $(ano).prop('max','4');
                        if($(ano).val() > 4){
                            $(ano).prop('value','4');
                        }
                    }
                });

                telInput.intlTelInput({
                    allowDropdown: false,
                    onlyCountries: ['ao'],
                    utilsScript  : "../js/build/js/utils.js",
                });


                var invalidFields       = (array)=> {
                    $(array).each(function (key, obj) {
                        $(obj).addClass('is-invalid');
                    });
                }

                var clearInvalidFields  = (array)=> {
                    $(array).each(function (key, obj) {
                        $(obj).on('keyup change',function () {
                            $(obj).removeClass('is-invalid')
                        });
                    });
                }

                var retriveFormData = ( serializedArray ) => {
                    var ob = {}
                    $(serializedArray).each(function (key, obj) {
                        if(key != 0 && key != 6){
                            ob[obj.name] = obj.value;
                        }
                    })

                   return ob;
                }

                $("#look-"+id+"").click(function (event){
                    event.preventDefault();
                    var invalidFieldsArray = [];
                    if(!telInput.intlTelInput("isValidNumber")){
                        invalidFieldsArray.push(telInput);
                    }

                    if(numeroEstudante.val().length != 8){
                        invalidFieldsArray.push(numeroEstudante);
                    }else if(!numeroEstudante.val().match(/2{1}0{1}1{1}[2-9]{1}-?\d{4}/)){
                        invalidFieldsArray.push(numeroEstudante);
                    }

                    if(name.val().length <= 3){
                        invalidFieldsArray.push(name);
                    }

                    if(email.val().length <= 0){
                        invalidFieldsArray.push(email);
                    }

                    if(invalidFieldsArray.length > 0){
                        event.preventDefault();
                        invalidFields(invalidFieldsArray);
                        alert("Insira campos válidos");
                        clearInvalidFields(invalidFieldsArray);
                    }else{
                        var formData    = $("#form-"+id+"");
                        var dadosAluno  = JSON.stringify(retriveFormData(formData.serializeArray()));
                        var hiddenInput = '<input type="hidden" name="dados_aluno" id="dados_aluno"/>'
                        formData.append(hiddenInput);
                        $("#dados_aluno").val(dadosAluno)
                        $("#submitButton-"+id+"").click();
                    }


                })



            })
        })
    </script>
</body>
</html>