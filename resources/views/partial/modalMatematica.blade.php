@foreach($actividades as $actividade)
    <div class="modal fade" id="modal-{{$actividade->id}}" tabindex="-1" role="dialog" aria-labelledby="modalMatematica" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-secondary">
                    <h5 class="text-warning col-11 text-center">Inscrição {{$actividade->nome}}</h5>
                    <button type="button" class="close col-1 text-right" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('create-inscricao',$actividade->id)}}" method="POST" id="form-{{$actividade->id}}">
                        @csrf
                        <div class="form-group row">
                            <label for="nome" class="col-md-4 col-form-label text-md-right text-dark">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="nome-{{$actividade->id}}" type="text" class="form-control{{ $errors->has('nome') ? ' is-invalid' : '' }}" name="nome" value="{{ old('nome') }}" required autofocus>

                                @if ($errors->has('nome'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right text-dark">Curso</label>
                            <div class="col-md-6">
                                <select name="curso" id="curso-{{$actividade->id}}" class="custom-select" required>
                                    <optgroup label="Departamento de Engenharia e Tecnologias">
                                        <option value="Engenharia Química">Engenharia Química</option>
                                        <option value="Engenharia Mecânica">Engenharia Mecânica</option>
                                        <option value="Engenharia Civil">Engenharia Civil</option>
                                        <option value="Engenharia Electrotécnica">Engenharia Electrotécnica</option>
                                        <option value="Engenharia de Produção Industrial">Engenharia de Produção Industrial</option>
                                        <option value="Engenharia Informática">Engenharia Informática</option>
                                    </optgroup>
                                    <optgroup label="Departamento de Ciências Sociais Aplicadas">
                                        <option value="Gestão de Empresas">Gestão de Empresas</option>
                                        <option value="Contabilidade">Contabilidade</option>
                                        <option value="Economia">Economia</option>
                                    </optgroup>
                                    <optgroup label="Departamento de Geociências">
                                        <option value="Geofísica">Geofísica</option>
                                        <option value="Engenharia de Petróleos">Engenharia de Petróleos</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ano" class="col-md-4 col-form-label text-md-right text-dark">Ano</label>
                            <div class="col-md-6">
                                <input type="number" min="1" max="5" id="ano-{{$actividade->id}}" class="form-control {{$errors->has('ano')? 'is-invalid' : ''}}" name="ano" value="{{old('ano')}}" required>

                                @if($errors->has('ano'))
                                    <span class="invalid-feedback">
                                <strong>{{$errors->first('ano')}}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right text-dark">E-mail</label>
                            <div class="col-md-6">
                                <input type="email" id="email-{{$actividade->id}}" class="form-control {{$errors->has('email')? 'is-invalid' : ''}}" name="email" value="{{old('email')}}" required>

                                @if($errors->has('email'))
                                    <span class="invalid-feedback">
                                <strong>{{$errors->first('email')}}</strong>
                            </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="telefone" class="col-md-4 col-form-label text-md-right text-dark">Telefone</label>
                            <div class="col-md-6">
                                <input type="tel" class="form-control {{$errors->has('telefone')? 'is-invalid' : ''}}" name="telefone" id="telefone-{{$actividade->id}}" value="{{old('telefone')}}" required>
                                @if($errors->has('telefone'))
                                    <span class="invalid-feedback">
                                <strong>{{$errors->first('telefone')}}</strong>
                            </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="numeroEstudante" class="col-md-4 col-form-label text-md-right text-dark">Nº Estudante</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control {{$errors->has('numeroEstudante')? 'is-invalid' : ''}}" name="numeroEstudante" id="numeroEstudante-{{$actividade->id}}" value="{{old('numeroEstudante')}}" required>

                                @if($errors->has('numeroEstudante'))
                                    <span class="invalid-feedback">
                                <strong>{{$errors->first('numeroEstudante')}}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div style="display: none">
                            <button type="submit" id="submitButton-{{$actividade->id}}"></button>
                        </div>
                    </form>

                    <div class="modal-footer">
                        <button type="button" id="look-{{$actividade->id}}" class="btn btn-outline-warning">Inscrever-se</button>
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger">Cancelar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endforeach