@extends('layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success alert-dismissable fade show text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('status')}}
        </div>
    @endif

    @if (session('alert'))
        <div class="alert alert-danger alert-dismissable fade show text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{session('alert')}}
        </div>
    @endif

    <section id="form-section">
        <div class="container">
            <div class="justify-content-center">
                <div class="card">
                    <div class="card-header text-center">
                        Configuração de Dados de Página Inicial
                    </div>

                    <div class="card-body">
                        <form action="{{route('update-inicial')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{method_field('put')}}
                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="titulo" class="col-form-label">Título <span class="text-danger">*</span></label>
                                    <input type="text" name="titulo" class="form-control" id="titulo" value="{{$config->titulo}}" required>
                                </div>

                                <div class="col-6">
                                    <label for="bg_img" class="col-form-label">Imagem de fundo<span class="text-danger">*</span></label>
                                    @if($config->background_image_path)
                                        <input type="text" name="bg_img" class="form-control" id="bg_img" value="{{$config->background_image_path}}" required>
                                    @else
                                        <input type="file" name="bg_img" class="form-control" id="bg_img" required>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-lg-12 col-md-12 col-sm-12 py-2">
                                    <label for="texto_principal">Texto Princípal<span class="text-danger">*</span></label>
                                    <textarea name="texto_principal" id="texto_principal" class="form-control" cols="30" rows="5" required>{{$config->texto_principal}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-success">
                                        Actualizar
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script src="{{asset('js/jquery/jquery.js')}}"></script>
    <script>
        $(document).ready(function () {
            var oldValue = '';
            var counter  = 0;
            var checkInputType = ()=> {
                $("#bg_img").click(function (event) {
                    if($(this).prop("type") !== 'file'){
                        $(this).prop("type",'file');
                    }
                })
            }

            checkInputType();

        });
    </script>
@stop