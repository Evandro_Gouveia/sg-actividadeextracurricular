<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PaginaInicialController@index');

Route::prefix('public/')->group(function (){
    Route::get('configuracao','PaginaInicialController@showConfigForm')->name('inicial-config');
    Route::put('update','PaginaInicialController@config')->name('update-inicial');
});

Route::prefix('inscricao/')->group(function (){
   Route::post('{id}/create','InscricaoController@create')->name('create-inscricao');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
