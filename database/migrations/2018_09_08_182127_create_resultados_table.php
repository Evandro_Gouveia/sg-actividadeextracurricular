<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fase_id')->unsigned();
            $table->integer('inscricao_id')->unsigned();
            $table->integer('nota')->nullable();
            $table->foreign('inscricao_id')->references('id')->on('inscricaos')->onDelete('cascade');
            $table->foreign('fase_id')->references('id')->on('fases')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados');
    }
}
